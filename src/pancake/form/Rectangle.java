package pancake.form;

import pancake.api.Drawable;
import pancake.api.Savable;

import java.awt.*;
import java.awt.Point;
import java.io.FileWriter;
import java.io.IOException;
/**
 * Represents a rectangle
 */
public class Rectangle implements Drawable,Savable {

    private  boolean valid = false;
    private  int topLeftX;
    private  int topLeftY;
    private  int height;
    private  int width;

    private Color contentColor;
    private  String textContent=" ";
    private  Color textColor=new Color(0,0,0);
    private Color contourColor = new Color(0,0,0);
    private  Font font = new Font(null,Font.BOLD,15);
    /**
     * Create a valid circle object
     * @param        topLeftX the x-coordinate of the top corn
     * @param        topLeftY the y-coordinate of the top corn
     * @param        height  the height of rectangle
     * @param        width  the width of rectangle
     */
    public Rectangle (int topLeftX, int topLeftY, int width, int height) {
        this.topLeftX = topLeftX;
        this.topLeftY = topLeftY;
        this.height = height;
        this.width = width;
        this.valid = true;
    }
    /**
     * Create a invalid rectangle object and call load to make it valid
     */
    public Rectangle ()
    {
        valid = false;
    }
    /**
     * draw a new rectangle object based on scalar number
     * @param         g <code>Graphics</code> object
     */
    @Override
    public void paint (Graphics g) {
        if(valid) {
            g.setColor(contentColor);
            g.fillRect(topLeftX, topLeftY, width, height);
            g.setColor(contourColor);
            g.drawRect(topLeftX, topLeftY, width, height);

            g.setColor(textColor);
            g.setFont(font);
            FontMetrics fm = g.getFontMetrics();
            int msgWidth = fm.stringWidth(textContent);
            int dx = (width - msgWidth) / 2;
            int dy = (height + fm.getAscent()) / 2;
            g.drawString(textContent, (topLeftX + dx), (topLeftY + dy));
        }
    }

    /**
     * scale or change the size of selected circle object based on scalar number
     * @param         scalar a double number used to scale element
     *
     */
    @Override
    public void scale (double scalar) {
        topLeftX= (int) (topLeftX-(scalar-1)*width/2);
        topLeftY= (int) (topLeftY-(scalar-1)*height/2);
        width=  (int) (width*scalar);
        height=  (int) (height*scalar);
    }
    /**
     * search the center of rectangle
     */
    @Override
    public Point searchCenter () {
        int Xcenter=topLeftX+(width)/2;
        int  Ycenter=topLeftY+(height)/2;
        Point p=new Point(Xcenter,Ycenter);
        return p;
    }
    /**
     * save  the new rectangle object to file
     * @param        writer file-object.
     *
     */
    @Override
    public void save (FileWriter writer) throws IOException  {
            writer.write(contentColor.getRed()+";"+contentColor.getGreen()+";"+contentColor.getBlue()+";"+ contourColor.getRed()+";"+ contourColor.getGreen()+";"+ contourColor.getBlue()+";"+topLeftX+";"+topLeftY+";"+width+";"+height+";Rectangle\n");
    }
    /**
     * load  the saved rectangle object from file
     * @param         part each part is separated by a semicolon
     */
    @Override
    public void load (String[] part) {
        int redContent= Integer.parseInt(part[0]);
        int greenContent= Integer.parseInt(part[1]);
        int blueContent= Integer.parseInt(part[2]);
        contentColor = new Color(redContent,greenContent,blueContent);
        int redContour= Integer.parseInt(part[3]);
        int greenContour= Integer.parseInt(part[4]);
        int blueContour= Integer.parseInt(part[5]);
        contourColor = new Color(redContour,greenContour,blueContour);
        topLeftX= Integer.parseInt(part[6]);
        topLeftY= Integer.parseInt(part[7]);
        width= Integer.parseInt(part[8]);
        height= Integer.parseInt(part[9]);
        valid = true;

    }

    /**
     * move rectangle object by offset x and y
     */
    @Override
    public void move (int x, int y) {
        topLeftX=topLeftX+x;
        topLeftY=topLeftY+y;
    }

    /**
     *     The set method takes a parameter (newContentColor) and assigns it to the color variable.
     *     The this keyword is used to refer to the current object.
     * @param         newContentColor new value of text object
     */
    @Override
    public void setBackgroundColor (Color newContentColor) {
        this.contentColor =newContentColor;
    }
    /**
     *     The set method takes a parameter (newContourColor) and assigns it to the color variable.
     *     The this keyword is used to refer to the current object.
     * @param         newContourColor new value of text object
     */
    public void setContourColor (Color newContourColor)
    {
        this.contourColor =newContourColor;
    }

    /**
     * get method returns the value of the variable color (content)
     */
    @Override
    public Color getBackgroundColor () {
        return this.contentColor;
    }

    /**
     * get method returns the value of the variable color (contour)
     */
    @Override
    public Color getContourColor () {
        return this.contourColor;
    }
    /**
     *     The set method takes a parameter (newText) and assigns it to the text variable.
     *     The this keyword is used to refer to the current object.
     * @param         newText new value of text object
     */
    @Override
    public void setText (String newText) {
         this.textContent=newText;
    }
    /**
     *     check if the mouse clicked in the area of circle
     * @param         x the x-coordinate of the mouse-click
     * @param         y the y-coordinate of the mouse-click
     */
    @Override
    public boolean checkIfMouseInArea(int x, int y){
            int  topLeftX= this.topLeftX;
            int  topLeftY= this.topLeftY;
            int  H= this.height+topLeftY;
            int  W= this.width+topLeftX;
            if(x>topLeftX && x<W && y>topLeftY && y<H){

                return true;
            }
        return false;
    }


}
