package pancake.form;

import pancake.api.Drawable;
import pancake.api.Savable;

import java.awt.*;
import java.awt.Point;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Represents a circle of a given radius
 */
public class Circle implements Drawable,Savable{
    private int radius;
    private int centerX;
    private int centerY;
    private  boolean valid = false;
    /**
     *     initialize the used color of content,contour and text
     */
    private Color textColor=new Color(0,0,0);
    private Color contentColor;
    private Color contourColor = new Color(0,0,0);

    private String textContent=" ";
    private Font font = new Font(null,Font.BOLD,15);
    /**
     * Create a valid circle object
     * @param        radius the a radius of a circle or sphere
     *               is any of the line segments from its center to its perimeter
     * @param        centerX the x-coordinate of the center
     * @param        centerY  the y-coordinate of the center
     */
    public Circle (int centerX, int centerY, int radius) {

        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
        this.valid = true;
    }
    /**
     * Create a invalid circle object and call load to make it valid
     */
    public Circle(){
        valid = false;
    }

    /**
     * scale or change the size of selected circle object based on scalar number
     * @param         scalar a double number used to scale element
     *
     */
    @Override
    public void scale (double scalar) {
        radius= (int) (radius*scalar);
    }

    /**
     * move circle object by offset x and y
     */
    @Override
    public void move (int x, int y) {
        centerX=centerX+x;
        centerY=centerY+y;
    }
    /**
     * save  the new circle object to file
     * @param        writer file-object.
     *
     */
    @Override
    public void save (FileWriter writer)throws IOException {
          writer.write(contentColor.getRed()+";"+contentColor.getGreen()+";"+contentColor.getBlue()+";"+ contourColor.getRed()+";"+ contourColor.getGreen()+";"+ contourColor.getBlue()+";"+centerX+";"+centerY+";"+radius+";Circle\n");
    }

    /**
     * load  the saved circle object from file
     * @param         part each part is separated by a semicolon
     */
    @Override
    public void load (String[] part) {
        int redContent= Integer.parseInt(part[0]);
        int greenContent= Integer.parseInt(part[1]);
        int blueContent= Integer.parseInt(part[2]);
        contentColor = new Color(redContent,greenContent,blueContent);

        int redContour= Integer.parseInt(part[3]);
        int greenContour= Integer.parseInt(part[4]);
        int blueContour= Integer.parseInt(part[5]);
        contourColor = new Color(redContour,greenContour,blueContour);
        centerX = Integer.parseInt(part[6]);
        centerY = Integer.parseInt(part[7]);
        radius = Integer.parseInt(part[8]);
        valid = true;
    }

    /**
     * search the center of circle
     */
    @Override
    public Point searchCenter () {
        Point p=new Point(centerX,centerY);
        return p;
    }
    /**
     * draw a new circle object based on scalar number
     * @param         g <code>Graphics</code> object
     */

    @Override
    public void paint (Graphics g) {

        g.setColor(contentColor);
        g.fillOval(centerX-radius, centerY-radius,2*radius,2*
                radius);
        g.setColor(contourColor);
        g.drawOval(centerX-radius, centerY-radius,2*radius,2*
                radius);
        g.setColor(textColor);
        g.setFont(font);
        FontMetrics fm = g.getFontMetrics();
        int msgWidth= fm.stringWidth(textContent);
        int msgHeight= fm.getAscent();
        int dx =  msgWidth/2;
        int dy = msgHeight/2;
        g.drawString(textContent,centerX-dx, centerY+dy);
    }

    /**
     *     The set method takes a parameter (newText) and assigns it to the text variable.
     *     The this keyword is used to refer to the current object.
     * @param         newText new value of text object
     */
    @Override
    public void setText (String newText) {
     this.textContent=newText;
    }

    /**
     *     The set method takes a parameter (newContentColor) and assigns it to the color variable.
     *     The this keyword is used to refer to the current object.
     * @param         newContentColor new value of text object
     */

    @Override
    public void setBackgroundColor (Color newContentColor) {
        this.contentColor =newContentColor;
    }

    /**
     *     The set method takes a parameter (newContourColor) and assigns it to the color variable.
     *     The this keyword is used to refer to the current object.
     * @param         newContourColor new value of text object
     */
    @Override
    public void setContourColor (Color newContourColor) {
        this.contourColor =newContourColor;
    }

    /**
     * get method returns the value of the variable color (content)
     */

    @Override
    public Color getBackgroundColor () {
        return this.contentColor;
    }

    /**
     * get method returns the value of the variable color (contour)
     */
    @Override
    public Color getContourColor () {
        return null;
    }

    /**
     *     The distance between the center and the point
     * @param         xp the x-coordinate of point p
     * @param         yp the y-coordinate of point p
     */
    public double distance(int xp, int yp)
    {
        double distance = Math.sqrt(((xp - centerX) * (xp - centerX)) + ((yp - centerY) * (yp - centerY)));
        return distance;
    }

    /**
     *     check if the mouse clicked in the area of circle
     * @param         x the x-coordinate of the mouse-click
     * @param         y the y-coordinate of the mouse-click
     */
    @Override
    public boolean checkIfMouseInArea(int x, int y){
       if(distance(x,y)<=radius) {
           return true;
       }
        return false;
    }


}
