package pancake.form;

import pancake.api.Drawable;
import pancake.api.Savable;

import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Represents a arrow
 */
public class Arrow implements Drawable,Savable {

    private  boolean valid = false;
    /** topLeftX the x-coordinate of the top corn*/
    private  int topLeftX;
    /** topLeftY the y-coordinate of the top corn*/
    private  int topLeftY;
    /** height  the height of rectangle*/
    private  int height;
    /** height  the width of rectangle*/
    private  int width;
    /*arrayX the array of x-coordinate of three points of Triangle*/
    private int[] arrayX;
    /*arrayY the array of y-coordinate of three points of Triangle */
    private int[] arrayY;

    private Color contentColor;
    private  String textContent=" ";
    private  Color textColor=new Color(0,0,0);
    private Color contourColor = new Color(0,0,0);
    private  Font font = new Font(null,Font.BOLD,15);
    /**
     * Create a valid arrow object
     * @param        topLeftX the x-coordinate of the top corn
     * @param        topLeftY the y-coordinate of the top corn
     * @param        height  the height of arrow
     * @param        width  the width of arrow
     */
    public Arrow (int topLeftX, int topLeftY, int width, int height) {
        this.topLeftX = topLeftX;
        this.topLeftY = topLeftY;
        this.height = height;
        this.width = width;
        this.valid = true;
    }
    /**
     * Create a invalid arrow object and call load to make it valid
     */
    public Arrow ()
    {
        valid = false;
    }
    /**
     * draw a new arrow object based on scalar number
     * @param         g <code>Graphics</code> object
     */
    @Override
    public void paint (Graphics g) {
        if(valid) {
             arrayX = new int[] {topLeftX + width, topLeftX + width*2/3, topLeftX + width*2/3};
             arrayY = new int[]  {topLeftY + height * 1/2, topLeftY , topLeftY +height};
            g.setColor(contentColor);
            g.fillRect(topLeftX , topLeftY + height * 1/4, width*2/3 , height * 1/2);

            g.setColor(contourColor);
            g.drawRect(topLeftX , topLeftY + height * 1/4, width*2/3 , height * 1/2);


            g.setColor(contentColor);
            g.fillPolygon(arrayX, arrayY, 3);

            g.setColor(contourColor);
            g.drawPolygon(arrayX, arrayY, 3);

            g.setColor(textColor);
            g.setFont(font);
            FontMetrics fm = g.getFontMetrics();
            int msgWidth = fm.stringWidth(textContent);
            int dx = (width - msgWidth) / 2;
            int dy = (height + fm.getAscent()) / 2;
            g.drawString(textContent, (topLeftX + dx), (topLeftY + dy));

            g.setColor(contentColor);

            g.fillRect(topLeftX + width*2/3 -5 , topLeftY+height/4+1, width/6 , height * 1/2-1);
        }
    }
    /**
     * scale or change the size of selected arrow object based on scalar number
     * @param         scalar a double number used to scale element
     *
     */
    @Override
    public void scale (double scalar) {
        topLeftX= (int) (topLeftX-(scalar-1)*width/2);
        topLeftY= (int) (topLeftY-(scalar-1)*height/2);
        width=  (int) (width*scalar);
        height=  (int) (height*scalar);
    }
    /**
     * search the center of arrow
     */
    @Override
    public Point searchCenter () {
        int Xcenter=topLeftX+(width)/2;
        int  Ycenter=topLeftY +(height)/2;
        Point p=new Point(Xcenter,Ycenter);
        return p;
    }
    /**
     * save  the new arrow object to file
     * @param        writer file-object.
     *
     */
    @Override
    public void save (FileWriter writer) throws IOException  {
            writer.write(contentColor.getRed()+";"+contentColor.getGreen()+";"+contentColor.getBlue()+";"+ contourColor.getRed()+";"+ contourColor.getGreen()+";"+ contourColor.getBlue()+";"+topLeftX+";"+topLeftY+";"+width+";"+height+";Arrow\n");
    }
    /**
     * load  the saved arrow object from file
     * @param         part each part is separated by a semicolon
     */
    @Override
    public void load (String[] part) {
        int redContent= Integer.parseInt(part[0]);
        int greenContent= Integer.parseInt(part[1]);
        int blueContent= Integer.parseInt(part[2]);
        contentColor = new Color(redContent,greenContent,blueContent);
        int redContour= Integer.parseInt(part[3]);
        int greenContour= Integer.parseInt(part[4]);
        int blueContour= Integer.parseInt(part[5]);
        contourColor = new Color(redContour,greenContour,blueContour);
        topLeftX= Integer.parseInt(part[6]);
        topLeftY= Integer.parseInt(part[7]);
        width= Integer.parseInt(part[8]);
        height= Integer.parseInt(part[9]);
        valid = true;

    }

    /**
     * move arrow object by offset x and y
     */
    @Override
    public void move (int x, int y) {
        topLeftX=topLeftX+x;
        topLeftY=topLeftY+y;
    }

    /**
     *     The set method takes a parameter (newContentColor) and assigns it to the color variable.
     *     The this keyword is used to refer to the current object.
     * @param         newContentColor new value of text object
     */
    @Override
    public void setBackgroundColor (Color newContentColor) {
        this.contentColor =newContentColor;
    }
    /**
     *     The set method takes a parameter (newContourColor) and assigns it to the color variable.
     *     The this keyword is used to refer to the current object.
     * @param         newContourColor new value of text object
     */
    public void setContourColor (Color newContourColor)
    {
        this.contourColor =newContourColor;
    }

    /**
     * get method returns the value of the variable color (content)
     */
    @Override
    public Color getBackgroundColor () {
        return this.contentColor;
    }

    /**
     * get method returns the value of the variable color (contour)
     */
    @Override
    public Color getContourColor () {
        return this.contourColor;
    }
    /**
     *     The set method takes a parameter (newText) and assigns it to the text variable.
     *     The this keyword is used to refer to the current object.
     * @param         newText new value of text object
     */
    @Override
    public void setText (String newText) {
         this.textContent=newText;
    }
    /**
     *     method to return the area of arrow
     */
    public double area(int x1, int y1, int x2, int y2, int x3, int y3)
    {
        return Math.abs((x1*(y2-y3) + x2*(y3-y1)+ x3*(y1-y2))/2.0);
    }
    /**
     *     check if the mouse clicked in the area of arrow
     * @param         x the x-coordinate of the mouse-click
     * @param         y the y-coordinate of the mouse-click
     */
    @Override
    public boolean checkIfMouseInArea(int x, int y){

            if(checkIfMouseInTriangle(x, y)||checkIfMouseInRectangle(x, y)){

                return true;
            }
        return false;
    }

    /**
     *     check if the mouse clicked in the area of rectangle
     * @param         x the x-coordinate of the mouse-click
     * @param         y the y-coordinate of the mouse-click
     */
    public boolean checkIfMouseInRectangle(int x, int y){
        int  H= width*2/3+topLeftY+ height * 1/4;
        int  W= height * 1/2+topLeftX;
        if((x>topLeftX && x<W && y>topLeftY && y<H)){

            return true;
        }
        return false;
    }
    /**
     *     check if the mouse clicked in the area of triangle
     * @param         x the x-coordinate of the mouse-click
     * @param         y the y-coordinate of the mouse-click
     */
    public boolean checkIfMouseInTriangle(int x, int y){
        /* Calculate area of triangle ABC */
        double A = area (arrayX[0], arrayY[0], arrayX[1], arrayY[1], arrayX[2], arrayY[2]);

        /* Calculate area of triangle PBC */
        double A1 = area (x, y, arrayX[1], arrayY[1], arrayX[2], arrayY[2]);

        /* Calculate area of triangle PAC */
        double A2 = area (arrayX[0], arrayY[0], x, y, arrayX[2], arrayY[2]);

        /* Calculate area of triangle PAB */
        double A3 = area (arrayX[0], arrayY[0], arrayX[1], arrayY[1], x, y);

        if((A == A1 + A2 + A3)){
            return true;
        }
        return false;
    }


}
