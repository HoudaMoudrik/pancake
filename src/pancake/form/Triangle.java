package pancake.form;

import pancake.api.Drawable;
import pancake.api.Savable;

import java.awt.*;
import java.awt.Point;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Represents a triangle
 */
public class Triangle implements Drawable,Savable {
    private boolean valid = false;
    private int[] arrayX = new int[3];
    private int[] arrayY= new int[3];

    private String textContent=" ";
    private Color textColor=new Color(0,0,0);
    private Color contentColor= new Color(0,0,0);
    private Color contourColor = new Color(0,0,0);
    private Font font = new Font(null,Font.BOLD,15);

    /**
     * Create a valid triangle object
     * @param        arrayX the array of x-coordinate of three points of Triangle
     * @param        arrayY the array of y-coordinate of three points of Triangle

     */
    public Triangle (int[] arrayX, int[] arrayY) {
        this.arrayX = arrayX;
        this.arrayY = arrayY;
        this.valid = true;
    }
    /**
     * Create a invalid triangle object and call load to make it valid
     */
    public Triangle(){
        valid = false;
    }
    @Override

    /**
     * scale or change the size of selected circle object based on scalar number
     * @param         scalar a double number used to scale element
     *
     */
    public void scale (double scalar) {
        Point p=searchCenter();
        int Xcenter= (int) p.getX();
        int Ycenter= (int) p.getY();
        for(int i=0;i<arrayX.length;++i){
             arrayX[i]= Xcenter+(int) ((arrayX[i]-Xcenter)*scalar);
        }
        for(int i=0;i<arrayY.length;++i){
             arrayY[i]= Ycenter+(int) ((arrayY[i]-Ycenter)*scalar);
        }
    }

    /**
     * search the center of triangle
     */
    @Override
    public Point searchCenter () {
        int cX=0;
        for(int i=0;i<arrayX.length;++i){
            int x=  arrayX[i]/3;
            cX=cX+x;
        }

        int cY=0;
        for(int i=0;i<arrayY.length;++i){
            int y=  arrayY[i]/3;
            cY=cY+y;
        }
        Point p=new Point(cX,cY);
        return p;
    }

    /**
     * save  the new circle object to file
     * @param        writer file-object.
     *
     */
    @Override
    public void save ( FileWriter writer) throws IOException {
      {

          writer.write(contentColor.getRed()+";"+contentColor.getGreen()+";"+contentColor.getBlue()+";"+ contourColor.getRed()+";"+ contourColor.getGreen()+";"+ contourColor.getBlue()+";");
            for(int i=0;i<arrayX.length;++i) {
                writer.write(arrayX[i]+";"+arrayY[i]+";");
            }
            writer.write("Triangle\n");
            }
    }

    /**
     * load  the saved circle object from file
     * @param         part each part is separated by a semicolon
     */
    @Override
    public void load (String[] part) {
        int redContent= Integer.parseInt(part[0]);
        int greenContent= Integer.parseInt(part[1]);
        int blueContent= Integer.parseInt(part[2]);
        contentColor = new Color(redContent,greenContent,blueContent);
        int redContour= Integer.parseInt(part[3]);
        int greenContour= Integer.parseInt(part[4]);
        int blueContour= Integer.parseInt(part[5]);
        contourColor = new Color(redContour,greenContour,blueContour);

        arrayX[0]= Integer.parseInt(part[6]);
        arrayY[0]= Integer.parseInt(part[7]);
        arrayX[1]= Integer.parseInt(part[8]);
        arrayY[1]= Integer.parseInt(part[9]);
        arrayX[2]= Integer.parseInt(part[10]);
        arrayY[2]= Integer.parseInt(part[11]);
        valid = true;
    }

    /**
     * draw a new circle object based on scalar number
     * @param         g <code>Graphics</code> object
     */
    @Override
    public void paint (Graphics g) {
        if(valid){
            g.setColor(contentColor);
            g.fillPolygon(arrayX,arrayY,3);
            g.setColor(contourColor);
            g.drawPolygon(arrayX,arrayY,3);

            int sumX=0;
            for(int i=0;i<arrayX.length;++i){
                int x=  arrayX[i]/3;
                sumX=sumX+x;
            }

            int sumY=0;
            for(int i=0;i<arrayY.length;++i){
                int y=  arrayY[i]/3;
                sumY=sumY+y;
            }

            g.setColor(textColor);
            g.setFont(font);
            FontMetrics fm = g.getFontMetrics();
            int msgWidth= fm.stringWidth(textContent);
            int msgHeight= fm.getAscent();
            int dx =  msgWidth/2;
            int dy = msgHeight/2;

            g.drawString(textContent,sumX-dx, sumY+dy);
        }
    }

    /**
     *     The set method takes a parameter (newContentColor) and assigns it to the color variable.
     *     The this keyword is used to refer to the current object.
     * @param         newContentColor new value of text object
     */

    @Override
    public void setBackgroundColor (Color newContentColor) {
        this.contentColor =newContentColor;
    }

    /**
     *     The set method takes a parameter (newText) and assigns it to the text variable.
     *     The this keyword is used to refer to the current object.
     * @param         newText new value of text object
     */
    @Override
    public void setText (String newText) {
        this.textContent=newText;
    }

    /**
     *     The set method takes a parameter (newContourColor) and assigns it to the color variable.
     *     The this keyword is used to refer to the current object.
     * @param         newContourColor new value of text object
     */
    public void setContourColor (Color newContourColor)
    {
       this.contourColor =newContourColor;
    }

    /**
     * move circle object by the offset x and y
     */
    @Override
    public void move (int x, int y) {

        for(int i=0;i<arrayX.length;++i){
            arrayX[i]=  arrayX[i]+x;

        }

        for(int i=0;i<arrayY.length;++i){
            arrayY[i]= arrayY[i]+y;
        }
    }

    /**
     * get method returns the value of the variable color (content)
     */
    @Override
    public Color getBackgroundColor () {
        return this.contentColor;
    }

    /**
     * get method returns the value of the variable color (contour)
     */
    @Override
    public Color getContourColor () {
        return this.contourColor;
    }

    /**
     *     method to return the area of triangle
     */
    public double area(int x1, int y1, int x2, int y2, int x3, int y3)
    {
        return Math.abs((x1*(y2-y3) + x2*(y3-y1)+ x3*(y1-y2))/2.0);
    }

    /**
     *     check if the mouse clicked in the area of triangle
     * @param         x the x-coordinate of the mouse-click
     * @param         y the y-coordinate of the mouse-click
     */
    @Override
    public boolean checkIfMouseInArea(   int x, int y)
    {
        /* Calculate area of triangle ABC */
        double A = area (arrayX[0], arrayY[0], arrayX[1], arrayY[1], arrayX[2], arrayY[2]);

        /* Calculate area of triangle PBC */
        double A1 = area (x, y, arrayX[1], arrayY[1], arrayX[2], arrayY[2]);

        /* Calculate area of triangle PAC */
        double A2 = area (arrayX[0], arrayY[0], x, y, arrayX[2], arrayY[2]);

        /* Calculate area of triangle PAB */
        double A3 = area (arrayX[0], arrayY[0], arrayX[1], arrayY[1], x, y);

        /* Check if sum of A1, A2 and A3 is same as A */
        return (A == A1 + A2 + A3);
    }



}
