package pancake.form;

import pancake.api.Drawable;
import pancake.api.Savable;

import java.awt.*;
import java.awt.Point;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Represents a Line
 */
public class Line implements Drawable,Savable {
    private int x1,y1,x2,y2;
    private boolean valid = false;
    private  Color contentColor;
    private Color contourColor;
    private String textContent=" ";
    private Color textColor=new Color(0,0,0);
    private Font font = new Font(null,Font.BOLD,15);

    /**
     * Create a valid line object
     * @param        x1 the x-coordinate of the first point of line
     * @param        y1 the y-coordinate of  the first point of line
     * @param        x2 the x-coordinate of  the second point of line
     * @param        y2  the y-coordinate of  the second point of line
     */
    public Line (int x1, int y1, int x2, int y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.valid = true;
    }
    /**
     * Create a invalid line object and call load to make it valid
     */
    public Line () {
        valid = false;

    }
    /**
     * draw a new line object based on scalar number
     * @param         g <code>Graphics</code> object
     */

    @Override
    public void paint (Graphics g) {
        if(valid) {
            g.setColor(contentColor);
            g.drawLine(x1,y1,x2,y2);

            g.setColor(textColor);
            g.setFont(font);
            FontMetrics fm = g.getFontMetrics();
            int msgWidth= fm.stringWidth(textContent);
            int msgHeight= fm.getAscent();
            int dx =  msgWidth/2;
            int dy = msgHeight/2;
            int centerX=(x1+x2)/2;
            int centerY=(y1+y2)/2;
            g.drawString(textContent,centerX-dx, centerY+dy);
        }

    }
    /**
     *     The set method takes a parameter (newContentColor) and assigns it to the color variable.
     *     The this keyword is used to refer to the current object.
     * @param         newContentColor new value of text object
     */
    @Override
    public void setBackgroundColor (Color newContentColor) {
        this.contentColor =newContentColor;
    }
    /**
     *     The set method takes a parameter (newContourColor) and assigns it to the color variable.
     *     The this keyword is used to refer to the current object.
     * @param         newContourColor new value of text object
     */
    public void setContourColor (Color newContourColor)
    {
        this.contourColor =newContourColor;
    }
    /**
     *     The set method takes a parameter (newText) and assigns it to the text variable.
     *     The this keyword is used to refer to the current object.
     * @param         newText new value of text object
     */
    @Override
    public void setText (String newText) {
     this.textContent=newText;
    }
    /**
     * get method returns the value of the variable color (content)
     */

    @Override
    public Color getBackgroundColor () {
        return this.contentColor;
    }
    /**
     * get method returns the value of the variable color (contour)
     */
    @Override
    public Color getContourColor () {
        return null;
    }
    /**
     * move line object by offset x and y
     */
    @Override
    public void move (int x, int y) {
        x1=x1+x;
        x2=x2+x;
        y1=y1+y;
        y2=y2+y;
    }
    /**
     * scale or change the size of selected line object based on scalar number
     * @param         scalar a double number used to scale element
     *
     */
    @Override
    public void scale (double scalar) {
        Point p=searchCenter();
        int Xcenter= (int) p.getX();
        int Ycenter= (int) p.getY();
        x1= Xcenter- (int) ((Xcenter-x1)*scalar);
        y1= Ycenter- (int) ((Ycenter-y1)*scalar);
        x2= Xcenter- (int) ((Xcenter-x2)*scalar);
        y2 =Ycenter- (int) ((Ycenter-y2)*scalar);
    }
    /**
     * search the center of line
     */
    @Override
    public Point searchCenter(){
        int Xcenter=(x1+x2)/2;
        int  Ycenter=(y1+y2)/2;
        Point p=new Point(Xcenter,Ycenter);
        return p;
    }
    /**
     * save  the new line object to file
     * @param        writer file-object.
     *
     */
    @Override
    public void save (FileWriter writer) throws IOException {
        writer.write(contentColor.getRed()+";"+contentColor.getGreen()+";"+contentColor.getBlue()+";"+ contourColor.getRed()+";"+ contourColor.getGreen()+";"+ contourColor.getBlue()+";"+x1+";"+y1+";"+x2+";"+y2+";Line\n");
    }
    /**
     * load  the saved line object from file
     * @param         part each part is separated by a semicolon
     */
    @Override
    public void load (String[] part) {
        int redContent= Integer.parseInt(part[0]);
        int greenContent= Integer.parseInt(part[1]);
        int blueContent= Integer.parseInt(part[2]);
        contentColor = new Color(redContent,greenContent,blueContent);
        int redContour= Integer.parseInt(part[3]);
        int greenContour= Integer.parseInt(part[4]);
        int blueContour= Integer.parseInt(part[5]);
        contourColor = new Color(redContour,greenContour,blueContour);

        x1= Integer.parseInt(part[6]);
        y1= Integer.parseInt(part[7]);
        x2= Integer.parseInt(part[8]);
        y2= Integer.parseInt(part[9]);
        valid = true;

    }

    /**
     * method to find Distance Segment
     */
    public double FindDistanceSegment (float xt,float yt,float x1, float y1, float x2, float y2)
    {
        float Xclosest;
        float Yclosest;
        float dx=x2-x1;
        float dy=y2-y1;
        if ((dx == 0) && (dy == 0))
        {
            dx =xt - x1;
            dy = yt - y1;
            return Math.sqrt(dx * dx + dy * dy);
        }
        float t = ((xt - x1) * dx + (yt - y1) * dy) / (dx * dx + dy * dy);

        if (t < 0)
        {

            dx = xt - x1;
            dy = yt - y1;
        }
        else if (t > 1)
        {
            dx = xt - x2;
            dy = yt - y2;
        }
        else
        {
            Xclosest = (x1+t*dx);
            Yclosest = (y1+t*dy);
            dx = xt - Xclosest;
            dy = yt - Yclosest;
        }
        double distance = Math.sqrt(dx * dx + dy * dy);
        return distance;
    }
    /**
     *     check if the mouse clicked in the area of circle
     * @param         x the x-coordinate of the mouse-click
     * @param         y the y-coordinate of the mouse-click
     */
    @Override
    public boolean checkIfMouseInArea (int x, int y) {
        double D = FindDistanceSegment(x,y,x1, y1, x2, y2);

        if(D<=4) return true;
        else return false;
    }


}
