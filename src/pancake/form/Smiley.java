package pancake.form;

import pancake.api.Drawable;
import pancake.api.Savable;

import java.awt.*;
import java.awt.Point;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Represents a smiley
 */
public class Smiley implements Drawable,Savable{
    private boolean valid =false;
    /*x-coordinate of center in the head of smiley*/
    private int centerXOfHead;
    /*y-coordinate of center in the head of smiley*/
    private int centerYOfHead;
    /*radius  of smiley*/
    private  int radius;
    /*x-coordinate of center in the left eye*/
    private  int XcenterOfTheLeftEye;
    /*y-coordinate of center in the left eye*/
    private  int YcenterOfTheLeftEye;
    /*x-coordinate of center in the right eye*/
    private int XcenterOfTheRightEye;
    /*y-coordinate of center in the right eye*/
    private int YcenterOfTheRightEye;
    /*radius of eye of smiley*/
    private int radiusOfTheEye;

    private  String textContent=" ";

    /**
     *     initialize the used color of content,contour and text
     */
    private  Color textColor=new Color(255,250,250);
    private Color contentColor= new Color(255,0,0);
    private Color contourColor = new Color(255,0,0);

    private  Font font = new Font(null,Font.BOLD,15);

    /**
     * Create a valid smile object
     * * @param        radius the a radius of a circle or sphere
     *                 is any of the line segments from its center to its perimeter
     * @param        centerXOfHead the array of x-coordinate of three points of Triangle
     * @param        centerYOfHead the array of y-coordinate of three points of Triangle

     */
    public Smiley (int centerXOfHead, int centerYOfHead, int radius) {
        this.centerXOfHead = centerXOfHead;
        this.centerYOfHead = centerYOfHead;
        this.radius=radius;
        XcenterOfTheLeftEye =centerXOfHead-radius/2;
        YcenterOfTheLeftEye =centerYOfHead-radius/4;
        XcenterOfTheRightEye=centerXOfHead+radius/2;
        YcenterOfTheRightEye=centerYOfHead-radius/4;
        radiusOfTheEye=radius/5;
        this.valid = true;
    }
    /**
     * Create a invalid smiley object and call load to make it valid
     */
    public Smiley () {
        valid = false;
    }

    /**
     * draw a new circle object based on scalar number
     * @param         g <code>Graphics</code> object
     */
    @Override
    public void paint (Graphics g) {
        if(valid){
            // face
            g.setColor(contentColor);

            g.fillOval(centerXOfHead-radius, centerYOfHead-radius,2*radius,2*
                    radius);
            g.setColor(contourColor);
            g.drawOval(centerXOfHead-radius, centerYOfHead-radius,2*radius,2*
                    radius);

            // left eye

            g.fillOval(XcenterOfTheLeftEye -radiusOfTheEye, YcenterOfTheLeftEye -radiusOfTheEye,2*radiusOfTheEye,2*
                    radiusOfTheEye);
            g.setColor(contourColor);
            g.drawOval(XcenterOfTheLeftEye -radiusOfTheEye, YcenterOfTheLeftEye -radiusOfTheEye,2*radiusOfTheEye,2*
                    radiusOfTheEye);

            //right eye
            g.fillOval(XcenterOfTheRightEye-radiusOfTheEye, YcenterOfTheRightEye-radiusOfTheEye,2*radiusOfTheEye,2*
                    radiusOfTheEye);
            g.setColor(contourColor);
            g.drawOval(XcenterOfTheRightEye-radiusOfTheEye, YcenterOfTheRightEye-radiusOfTheEye,2*radiusOfTheEye,2*
                    radiusOfTheEye);

            //mouth
            g.drawArc( centerXOfHead-radius/2, centerYOfHead-radius/4, radius, radius, 180, 180);
            g.setColor(contourColor);

            g.setFont(font);
            FontMetrics fm = g.getFontMetrics();
            int msgWidth= fm.stringWidth(textContent);
            int msgHeight= fm.getAscent();
            int dx =  msgWidth/2;
            int dy = msgHeight/2;
            g.drawString(textContent,centerXOfHead-dx, centerYOfHead+dy);

        }

    }
    /**
     * move smiley object by the offset x and y
     */
    @Override
    public void move (int x, int y) {
        centerXOfHead=centerXOfHead+x;
        centerYOfHead=centerYOfHead+y;
         XcenterOfTheLeftEye = XcenterOfTheLeftEye +x;
         YcenterOfTheLeftEye = YcenterOfTheLeftEye +y;
         XcenterOfTheRightEye=XcenterOfTheRightEye+x;
         YcenterOfTheRightEye=YcenterOfTheRightEye+y;

    }
    /**
     * scale or change the size of selected smiley object based on scalar number
     * @param         scalar a double number used to scale element
     *
     */
    @Override
    public void scale (double scalar) {
        radius= (int) (radius*scalar);
        XcenterOfTheLeftEye =centerXOfHead-radius/2;
        YcenterOfTheLeftEye =centerYOfHead-radius/4;
        XcenterOfTheRightEye=centerXOfHead+radius/2;
        YcenterOfTheRightEye=centerYOfHead-radius/4;
        radiusOfTheEye=radius/5;
    }
    /**
     * search the center of smiley
     */
    @Override
    public Point searchCenter () {

        Point p=new Point(centerXOfHead,centerYOfHead);
        return p;
    }
    /**
     *     The set method takes a parameter (newContentColor) and assigns it to the color variable.
     *     The this keyword is used to refer to the current object.
     * @param         newContentColor new value of text object
     */

    @Override
    public void setBackgroundColor (Color newContentColor) {
        this.contentColor =newContentColor;
    }
    /**
     *     The set method takes a parameter (newText) and assigns it to the text variable.
     *     The this keyword is used to refer to the current object.
     * @param         newText new value of text object
     */
    @Override
    public void setText (String newText) {
        this.textContent=newText;
    }
    /**
     *     The set method takes a parameter (newContourColor) and assigns it to the color variable.
     *     The this keyword is used to refer to the current object.
     * @param         newContourColor new value of text object
     */
    public void setContourColor (Color newContourColor)
    {
        this.contourColor=newContourColor;
    }
    /**
     * get method returns the value of the variable color (content)
     */

    @Override
    public Color getBackgroundColor () {
        return this.contentColor;
    }
    /**
     * get method returns the value of the variable color (contour)
     */
    @Override
    public Color getContourColor () {
        return this.contourColor;
    }

    /**
     * save  the new smiley object to file
     * @param        writer file-object.
     *
     */
    @Override
    public void save (FileWriter writer) throws IOException {
        writer.write(contentColor.getRed()+";"+contentColor.getGreen()+";"+contentColor.getBlue()+";"+
                contourColor.getRed()+";"+ contourColor.getGreen()+";"+ contourColor.getBlue()+";"+
                centerXOfHead +";"+ centerYOfHead+";"+radius+";Smiley\n");

    }

    /**
     * load  the saved smiley object from file
     * @param         part each part is separated by a semicolon
     */
    @Override
    public void load (String[] part) {
        int redContent= Integer.parseInt(part[0]);
        int greenContent= Integer.parseInt(part[1]);
        int blueContent= Integer.parseInt(part[2]);
        contentColor = new Color(redContent,greenContent,blueContent);
        int redContour= Integer.parseInt(part[3]);
        int greenContour= Integer.parseInt(part[4]);
        int blueContour= Integer.parseInt(part[5]);
        contourColor = new Color(redContour,greenContour,blueContour);
        centerXOfHead = Integer.parseInt(part[6]);
        centerYOfHead = Integer.parseInt(part[7]);
        radius = Integer.parseInt(part[8]);
        XcenterOfTheLeftEye =centerXOfHead-radius/2;
        YcenterOfTheLeftEye =centerYOfHead-radius/4;
        XcenterOfTheRightEye=centerXOfHead+radius/2;
        YcenterOfTheRightEye=centerYOfHead-radius/4;
        radiusOfTheEye=radius/5;
        valid = true;
    }

    /**
     *     The distance between the center and the point
     * @param         xp the x-coordinate of point p
     * @param         yp the y-coordinate of point p
     */
    public double distance(int xp, int yp)
    {
        double distance = Math.sqrt(((xp - centerXOfHead) * (xp - centerXOfHead)) + ((yp - centerYOfHead) * (yp - centerYOfHead)));
        return distance;
    }

    /**
     *     check if the mouse clicked in the area of circle
     * @param         x the x-coordinate of the mouse-click
     * @param         y the y-coordinate of the mouse-click
     */
    @Override
    public boolean checkIfMouseInArea(int x, int y){
        if(distance(x,y)<=radius) {
            return true;
        }
        return false;
    }
}
