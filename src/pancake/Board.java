package pancake;

import pancake.button.*;
import pancake.form.*;
import pancake.form.Rectangle;
import pancake.api.Callable;
import pancake.api.Drawable;
import pancake.api.Savable;

import javax.swing.*;
import java.awt.*;
import java.awt.Point;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;


public class Board extends JPanel {
    /** button to click Color*/
    private Colorado palette;
    /** button to click arrow button*/
    private ButtonArrow buttonArrow;
    /** button to click rectangle button*/
    private ButtonRectangle buttonRectangle;
    /** button to click triangle button*/
    private ButtonTriangle buttonTriangle;
    /** button to click move button*/
    private ButtonMove buttonMove;
    /** button to click circle button*/
    private ButtonCircle buttonCircle;
    /** button to click Line button*/
    private ButtonLine buttonLine;
    /** button to click Smile button*/
    private  ButtonSmile buttonSmile;
    /** button to click Leer button*/
    private ButtonErase buttonErase;
    /** button to click Move button*/
    private  ButtonPaint buttonPaint;
    /** button to click Text button*/
    private ButtonText buttonText;
    /** button to click Scale button*/
    private ButtonScale buttonScale;
    /** button to click buttonSave button*/
    private ButtonSave buttonSave;
    /** button to click buttonLoad button*/
    private ButtonLoad buttonLoad;

    /** coordinate of the first andd second point to beclicked*/
    private int xFirstPoint, yFirstPoint, xSecondPoint, ySecondPoint;
    /** define temporair Drawable*/
    private Drawable TmpDrawable;
    private double alpha,betha, scalar;

    /** center of the form*/
    private Point centerOfTheForm;

    /** content and contour of the form*/
    private Color contentOfForm, contourOfForm;

    /** initial state of button */
    private BUTTON guiState = BUTTON.GUI_DEFAULT;

    /** define and initialize the used color of content */
    private Color white = new Color(255, 255, 255);

    /** list of object to be drawed on the screen */
    private ArrayList<Drawable> drawableList = new ArrayList<Drawable>();
    /** list of object to be saved in the file */
    private ArrayList<Savable> savableList = new ArrayList<Savable>();
    /** list of object to be callable in the screen */
    private ArrayList<Callable> callableList = new ArrayList<Callable>();

    /** buttonSave form in file */
    public void saveTheForm () throws IOException {
     FileWriter fw=  new FileWriter("form.txt");
        for(Savable s:savableList)
        {
            s.save(fw);
        }
        fw.close();
    }
    /** buttonLoad form from file */
    public void loadTheForms () {

            try {
                /** file which used to buttonLoad data  */
                File file = new File("form.txt");
                Scanner inputStream = new Scanner(file);
                /** check if the file has next line  */
                while (inputStream.hasNextLine()) {
                    /** read next line  */
                    String data = inputStream.nextLine();
                    /** split the data of the correspond line  */
                    String[] part= data.split(";");
                    int size=part.length;
                    /** if the last part equals Line  */
                    if(part[size-1].equals("Line")){
                        Line newLine = new Line();
                        /** buttonLoad this line  */
                        newLine.load(part);
                        /** put this line in the drawable list  */
                        drawableList.add(0,newLine);
                    }
                    /** if the last part equals Rectangle  */
                    else if(part[size-1].equals("Rectangle")){
                        Rectangle newRectangle = new Rectangle();
                        /** buttonLoad this Rectangle  */
                        newRectangle.load(part);
                        /** put this Rectangle in the drawable list  */
                        drawableList.add(0,newRectangle);
                    }
                    /** if the last part equals arrow  */
                    else if(part[size-1].equals("Arrow")){
                        Arrow newArrow = new Arrow();
                        /** buttonLoad this Rectangle  */
                        newArrow.load(part);
                        /** put this Rectangle in the drawable list  */
                        drawableList.add(0,newArrow);
                    }
                    /** if the last part equals Rectangle  */
                    else if(part[size-1].equals("Smiley")){
                        Smiley newSmile = new Smiley();
                        /** buttonLoad this Rectangle  */
                        newSmile.load(part);
                        /** put this Rectangle in the drawable list  */
                        drawableList.add(0,newSmile);
                    }
                    /** if the last part equals Triangle  */
                    else     if(part[size-1].equals("Triangle")){
                        Triangle newTriangle=new Triangle();
                        /** buttonLoad this Triangle  */
                        newTriangle.load(part);
                        /** put this Triangle in the drawable list  */
                        drawableList.add(0,newTriangle);
                    }
                    /** if the last part equals Circle  */
                    else     if(part[size-1].equals("Circle")){
                        Circle newCircle = new Circle();
                        /** buttonLoad this Triangle  */
                        newCircle.load(part);
                        /** put this Circle in the drawable list  */
                        drawableList.add(0,newCircle);
                    }
                }

                inputStream.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }


    }

    /** Button is identifier of the enum with values */
    public enum BUTTON {
        GUI_DEFAULT,
        GUI_DRAW_LINE,
        GUI_DRAW_FIRSTPOINTOFLINE,
        GUI_DRAW_FIRSTPOINTOFTRIANGLE,
        GUI_DRAW_SECONDPOINTOFTRIANGLE,
        GUI_DRAW_FIRSTPOINTOFRECTANGLE,
        GUI_DRAW_FIRSTPOINTOFARROW,
        GUI_DRAW_CIRCLE,
        GUI_DRAW_CIRCLE_RADIUS,
        GUI_DRAW_RECTANGLE,
        GUI_DRAW_ARROW,
        GUI_DRAW_TRIANGLE,
        GUI_CHOOSE_COLOR,
        GUI_DRAW_SMILE_HEAD,
        GUI_DRAW_SMILE_NOSE,
        GUI_DRAW_TEXT,
        GUI_CHOOSE_ELEMENTTOBEDRAGED,
        GUI_CHOOSE_ELEMENTTOBESCALED,
        GUI_DRAG_ELEMENT,
        GUI_SCALE_ELEMENT,
        GUI_ERASE_SHAPES

    }
    /**  Dimension of board */

    public Dimension getPreferredSize () {
        return new Dimension(600, 400);
    }

    public Board () {

        //Colorado
        palette = new Colorado(400, 22, 20);
        callableList.add(palette);
        drawableList.add(palette);

        buttonRectangle = new ButtonRectangle(540, 22, 20, 20, this);
        drawableList.add(buttonRectangle);
        callableList.add(buttonRectangle);
        buttonArrow = new ButtonArrow(569, 66, 20, 20, this);
        drawableList.add(buttonArrow);
        callableList.add(buttonArrow);
        buttonCircle = new ButtonCircle( 540, 44, 20, this);
        drawableList.add(buttonCircle);
        callableList.add(buttonCircle);
        buttonSmile = new ButtonSmile(540, 66, 20, this);
        drawableList.add(buttonSmile);
        callableList.add(buttonSmile);
        buttonMove = new ButtonMove( 540, 88, 20, this);
        drawableList.add(buttonMove);
        callableList.add(buttonMove);
        buttonPaint = new ButtonPaint(540, 110, 20, this);
        drawableList.add(buttonPaint);
        callableList.add(buttonPaint);
        buttonSave = new ButtonSave( 540, 132, 20, this);
        drawableList.add(buttonSave);
        callableList.add(buttonSave);
        buttonErase = new ButtonErase( 540, 154, 20, this);
        drawableList.add(buttonErase);
        callableList.add(buttonErase);

        buttonTriangle = new ButtonTriangle(569, 22, 20, this);
        drawableList.add(buttonTriangle);
        callableList.add(buttonTriangle);
        buttonLine = new ButtonLine( 569, 44, 20, this);
        drawableList.add(buttonLine);
        callableList.add(buttonLine);

        buttonText = new ButtonText( 569, 88, 20, this);
        drawableList.add(buttonText);
        callableList.add(buttonText);
        buttonScale = new ButtonScale(569, 110, 20, this);
        drawableList.add(buttonScale);
        callableList.add(buttonScale);
        buttonLoad = new ButtonLoad( 569, 132, 20, this);
        drawableList.add(buttonLoad);
        callableList.add(buttonLoad);

        for (Drawable d : drawableList) {
            d.setContourColor(white);
            d.setBackgroundColor(white);
        }
        invalidate();
        repaint();
    }
    /**
     *  method  to paint component
     *  @param        g graphics

     */
    public void paintComponent (Graphics g) {
        super.paintComponent(g);
        for (Drawable d : drawableList) {

            d.paint(g);
        }
        for (Savable c : savableList) {
            c.paint(g);

        }
    }

    /**
     *  method  to click in the  place outside the forms
     *  @param        x x-coordinate
     *  @param        y y-coordinate
     */
    public BUTTON mouseClickDefault(int x,int y)
    {
        return BUTTON.GUI_DEFAULT;
    }

    /**
     *  method  to click in the first place in order to draw smile head
     *  @param        x x-coordinate
     *  @param        y y-coordinate
     */
    public BUTTON mouseClickPointOfHead (int x, int y)
    {
        xFirstPoint = x;
        yFirstPoint = y;
        return BUTTON.GUI_DRAW_SMILE_NOSE;
    }
    /**
     *  method  to click in the first place in order to draw smile head
     *  @param        x x-coordinate
     *  @param        y y-coordinate
     */
    public BUTTON mouseClickPointOfNose (int x, int y)
    {
        xSecondPoint = x;
        ySecondPoint = y;
        double radius=Math.sqrt((xSecondPoint-xFirstPoint)*(xSecondPoint-xFirstPoint)+(ySecondPoint-yFirstPoint)*(ySecondPoint-yFirstPoint));
        Smiley newSmily = new Smiley(xFirstPoint, yFirstPoint, (int) radius);

        newSmily.setContourColor(palette.getSavedContourColor());
        newSmily.setBackgroundColor(palette.getSavedContentColor());
        savableList.add(newSmily);
        drawableList.add(newSmily);
        return BUTTON.GUI_DEFAULT;
    }
    /**
     *  method  to click in the first place in order to draw line
     *  @param        x x-coordinate
     *  @param        y y-coordinate
     */

    public BUTTON mouseClickLinePoint1 (int x, int y)
    {
        xFirstPoint = x;
        yFirstPoint = y;
        return BUTTON.GUI_DRAW_FIRSTPOINTOFLINE;
    }

    /**
     *  method  to click in the place in order to draw text
     *  @param        x x-coordinate
     *  @param        y y-coordinate
     */
    public BUTTON mouseClickBeginOfText (int x, int y)
    {
        xFirstPoint = x;
        yFirstPoint = y;
        for (Drawable d : drawableList) {
            if (d.checkIfMouseInArea(xFirstPoint, yFirstPoint)) {
                String answer = askUser("give the text");
                    d.setText(answer);
            }
        }
        return BUTTON.GUI_DEFAULT;
    }
    /**
     *  method  to click in the form to be draged
     *  @param        x x-coordinate
     *  @param        y y-coordinate
     */
    public BUTTON saveTheCoordinateOfElementTobeDraged (int x, int y)
    {
        for (Drawable d : drawableList) {
            if (d.checkIfMouseInArea(x,y)) {
                xFirstPoint = x;
                yFirstPoint = y;
                TmpDrawable = d;
                return BUTTON.GUI_DRAG_ELEMENT;
            }

        }
        return BUTTON.GUI_DEFAULT;
    }
    /**
     *  method  to buttonSave The Coordinate Of Element To be Scaled
     *  @param        x x-coordinate
     *  @param        y y-coordinate
     */
    public BUTTON saveTheCoordinateOfElementTobeScaled (int x, int y)
    {
        for (Drawable d : drawableList) {
            if (d.checkIfMouseInArea(x,y)) {
                xFirstPoint = x;
                yFirstPoint = y;
                centerOfTheForm =d.searchCenter();
                alpha = Math.sqrt(((xFirstPoint - centerOfTheForm.getX()) * (xFirstPoint - centerOfTheForm.getX())) + ((yFirstPoint - centerOfTheForm.getY()) * (yFirstPoint - centerOfTheForm.getY())));
                TmpDrawable = d;
                return BUTTON.GUI_SCALE_ELEMENT;
            }
        }
        return BUTTON.GUI_DEFAULT;
    }

    /**
     *  method  to drag the selected form
     *  @param        x x-coordinate
     *  @param        y y-coordinate
     */
    private BUTTON dragElement (int x, int y) {

        TmpDrawable.move(x,y);
        return BUTTON.GUI_DEFAULT;
    }

    /**
     *  method  to scale the selected form
     *  @param        x x-coordinate
     *  @param        y y-coordinate
     */
    private BUTTON scaleElement (int x, int y) {
        betha = Math.sqrt(((x - centerOfTheForm.getX()) * (x - centerOfTheForm.getX())) + ((y - centerOfTheForm.getY()) * (y - centerOfTheForm.getY())));
        scalar = betha / alpha;
        scalar=scalar/2;
        TmpDrawable.scale(scalar);
        return BUTTON.GUI_DEFAULT;
    }

    /**
     *  method  to click in the second place in order to draw line
     *  @param        x x-coordinate
     *  @param        y y-coordinate
     */
    public BUTTON mouseClickLinePoint2 (int x, int y)
    {
        Line newLine = new Line(xFirstPoint, yFirstPoint, x, y);

        newLine.setContourColor(palette.getSavedContourColor());
        newLine.setBackgroundColor(palette.getSavedContentColor());
        savableList.add(newLine);
        drawableList.add(newLine);
        return BUTTON.GUI_DEFAULT;
    }

    /**
     *  method  to click in the first place in order to draw triangle
     *  @param        x x-coordinate
     *  @param        y y-coordinate
     */
    public BUTTON mouseClickTrianglePoint1 (int x, int y)
    {
        xFirstPoint = x;
        yFirstPoint = y;
        return BUTTON.GUI_DRAW_FIRSTPOINTOFTRIANGLE;
    }

    /**
     *  method  to click in the second place in order to draw triangle
     *  @param        x x-coordinate
     *  @param        y y-coordinate
     */
    public BUTTON mouseClickTrianglePoint2 (int x, int y)
    {
        xSecondPoint = x;
        ySecondPoint = y;
        return BUTTON.GUI_DRAW_SECONDPOINTOFTRIANGLE;
    }

    /**
     *  method  to click in the third place in order to draw triangle
     *  @param        x x-coordinate
     *  @param        y y-coordinate
     */
    public BUTTON mouseClickTrianglePoint3 (int x, int y)
    {
        int xpoints[] = {xFirstPoint, xSecondPoint, x};
        int ypoints[] = {yFirstPoint, ySecondPoint, y};
        Triangle newTriangle = new Triangle(xpoints, ypoints);

        newTriangle.setContourColor(palette.getSavedContourColor());
        newTriangle.setBackgroundColor(palette.getSavedContentColor());
        savableList.add(0,newTriangle);
        drawableList.add(0,newTriangle);
        return BUTTON.GUI_DEFAULT;
    }

    /**
     *  method  to click in the first place in order to draw rectangle
     *  @param        x x-coordinate
     *  @param        y y-coordinate
     */
    public BUTTON mouseClickRectanglePoint1 (int x, int y)
    {
        xFirstPoint = x;
        yFirstPoint = y;
        return BUTTON.GUI_DRAW_FIRSTPOINTOFRECTANGLE;
    }

    /**
     *  method  to click in the second place in order to draw rectangle
     *  @param        x x-coordinate
     *  @param        y y-coordinate
     */
    public BUTTON mouseClickRectanglePoint2 (int x, int y)
    {
        Rectangle newRectangle = new Rectangle(xFirstPoint, yFirstPoint, x - xFirstPoint, y - yFirstPoint);

        newRectangle.setContourColor(palette.getSavedContourColor());
        newRectangle.setBackgroundColor(palette.getSavedContentColor());
        savableList.add(newRectangle);
        drawableList.add(newRectangle);
        return BUTTON.GUI_DEFAULT;
    }
    /**
     *  method  to click in the first place in order to draw arrow
     *  @param        x x-coordinate
     *  @param        y y-coordinate
     */
    public BUTTON mouseClickArrowPoint1 (int x, int y)
    {
        xFirstPoint = x;
        yFirstPoint = y;
        return BUTTON.GUI_DRAW_FIRSTPOINTOFARROW;
    }
    /**
     *  method  to click in the first place in order to draw arrow
     *  @param        x x-coordinate
     *  @param        y y-coordinate
     */
    public BUTTON mouseClickArrowPoint2 (int x, int y)
    {
        int width=x - xFirstPoint;
        int height=y - yFirstPoint;
        Arrow newArrow = new Arrow(xFirstPoint, yFirstPoint, width, height);
        newArrow.setContourColor(palette.getSavedContourColor());
        newArrow.setBackgroundColor(palette.getSavedContentColor());
        savableList.add(newArrow);
        drawableList.add(newArrow);
        return BUTTON.GUI_DEFAULT;
    }

    /**
     *  method  to click in the place in order to draw circle
     *  @param        x x-coordinate
     *  @param        y y-coordinate
     */
    public BUTTON mouseClickCircleCenter (int x, int y)
    {
        xFirstPoint = x;
        yFirstPoint = y;
        return BUTTON.GUI_DRAW_CIRCLE_RADIUS;
    }

    /**
     *  method  to click in the form to colored
     *  @param        x x-coordinate
     *  @param        y y-coordinate
     */
    public BUTTON mouseClickCircleRadius (int x, int y)
    {
        int doppelDistance =(x- xFirstPoint)*(x- xFirstPoint)+(y- yFirstPoint)*(y- yFirstPoint);
        Circle newCircle = new Circle(xFirstPoint, yFirstPoint, (int) Math.sqrt(doppelDistance));

        newCircle.setContourColor(palette.getSavedContourColor());
        newCircle.setBackgroundColor(palette.getSavedContentColor());
        savableList.add(newCircle);
        drawableList.add(newCircle);
        return BUTTON.GUI_DEFAULT;
    }

    /**
     *  method  to click in the form to colored
     *  @param        x x-coordinate
     *  @param        y y-coordinate
     */
    private BUTTON mouseCLickTheForm (int x, int y) {
        for (Drawable d : drawableList) {
            if (d.checkIfMouseInArea(x, y)) {
                d.setBackgroundColor(contentOfForm);
                d.setContourColor(contourOfForm);
            }
        }
        return BUTTON.GUI_DEFAULT;
    }

    /**
     * void method to choose element to be draged
     */
    public void chooseElementToBeDraged()
    {
        guiState = BUTTON.GUI_CHOOSE_ELEMENTTOBEDRAGED;
    }

    /**
     * void method to choose element to be scaled
     */
    public void chooseElementToBeScaled()
    {
        guiState = BUTTON.GUI_CHOOSE_ELEMENTTOBESCALED;
    }

    /**
     * void method to start drawing line
     */
    public void startDrawLine()
    {
        guiState = BUTTON.GUI_DRAW_LINE;
    }

    /**
     * void method to start drawing smile
     */
    public void startDrawsmile()
    {
        guiState = BUTTON.GUI_DRAW_SMILE_HEAD;
    }

    /**
     * void method to start drawing triangle
     */
    public void startDrawTriangle()
    {
        guiState = BUTTON.GUI_DRAW_TRIANGLE;
    }

    /**
     * void method to start drawing rectangle
     */
    public void startDrawRectangle()
    {
        guiState = BUTTON.GUI_DRAW_RECTANGLE;
    }

    /**
     * void method to start drawing rectangle
     */
    public void startDrawArrow()
    {
        guiState = BUTTON.GUI_DRAW_ARROW;
    }
    /**
     * void method to start drawing circle
     */
    public void startDrawCircle()
    {
        guiState = BUTTON.GUI_DRAW_CIRCLE;
    }
    /**
     * void method to erase drawing shape
     */
    public void beginWithErasing()
    {
        guiState = BUTTON.GUI_ERASE_SHAPES;
    }
    /**
     * void method to erase drawing shape
     */
    public BUTTON eraseTheSelected(int x,int y)
    {
        Savable toBeRemoved = null;
        for (Savable s : savableList) {
            if(s.checkIfMouseInArea(x, y)) {
                toBeRemoved = s;
                break;
            }
        }

        if(toBeRemoved!=null)
        {
            savableList.remove(toBeRemoved);
            drawableList.remove(toBeRemoved);
        }

        return BUTTON.GUI_DEFAULT;
    }


    /**
     * void method to choose the color to be used in form
     */
    public void chooseTheColor()
    {
         contentOfForm=palette.getSavedContentColor();
         contourOfForm=palette.getSavedContourColor();
        guiState = BUTTON.GUI_CHOOSE_COLOR;
    }

    /**
     * void method to add the text  in the form
     */
    public void addText()
    {
        guiState = BUTTON.GUI_DRAW_TEXT;
    }

    /**
     * void method to implement differents buttons to be clicked
     * @param        x x-coordinate of button click
     * @param        y y-coordinate of button click
     */
    public void mousePressed (int x, int y) throws IOException {
        boolean clickConsumed = processCallablesClick(x,y);
        if(clickConsumed == false) {
            switch (guiState) {

                case GUI_DEFAULT:
                    guiState = mouseClickDefault(x, y);
                    break;
                case GUI_DRAW_SMILE_HEAD:
                    guiState = mouseClickPointOfHead(x, y);
                    break;
                case GUI_DRAW_SMILE_NOSE:
                    guiState = mouseClickPointOfNose(x, y);
                    break;
                case GUI_DRAW_LINE:
                    guiState = mouseClickLinePoint1(x, y);
                    break;
                case GUI_DRAW_FIRSTPOINTOFLINE:
                    guiState = mouseClickLinePoint2(x, y);
                    break;
                case GUI_DRAW_TRIANGLE:
                    guiState = mouseClickTrianglePoint1(x, y);
                    break;
                case GUI_DRAW_FIRSTPOINTOFTRIANGLE:
                    guiState = mouseClickTrianglePoint2(x, y);
                    break;
                case GUI_DRAW_SECONDPOINTOFTRIANGLE:
                    guiState = mouseClickTrianglePoint3(x, y);
                    break;
                case GUI_DRAW_ARROW:
                    guiState = mouseClickArrowPoint1(x, y);
                    break;
                case GUI_DRAW_FIRSTPOINTOFARROW:
                    guiState = mouseClickArrowPoint2(x, y);
                    break;
                case GUI_DRAW_RECTANGLE:
                    guiState = mouseClickRectanglePoint1(x, y);
                    break;
                case GUI_DRAW_FIRSTPOINTOFRECTANGLE:
                    guiState = mouseClickRectanglePoint2(x, y);
                    break;

                case GUI_DRAW_CIRCLE:
                    guiState=mouseClickCircleCenter(x,y);
                    break;
                case GUI_DRAW_CIRCLE_RADIUS:
                    guiState=mouseClickCircleRadius(x,y);
                    break;
                case GUI_CHOOSE_COLOR:
                    guiState=mouseCLickTheForm(x,y);
                    break;
                case GUI_DRAW_TEXT:
                    guiState = mouseClickBeginOfText(x, y);
                    break;
                case GUI_CHOOSE_ELEMENTTOBEDRAGED:
                    guiState = saveTheCoordinateOfElementTobeDraged(x, y);
                    break;
                case GUI_CHOOSE_ELEMENTTOBESCALED:
                    guiState = saveTheCoordinateOfElementTobeScaled(x, y);
                    break;
                case GUI_ERASE_SHAPES:
                    guiState=eraseTheSelected(x,y);
                    break;


           }
        }
        invalidate();
        repaint();
    }



    /**
     * void method to implement move button
     * @param        x x-coordinate of button click
     * @param        y y-coordinate of button click
     */
    public void mouseReleased (int x, int y)  {

            switch (guiState) {
                case GUI_DRAG_ELEMENT:
                    guiState = dragElement(x- xFirstPoint, y- yFirstPoint);
                    break;
                case GUI_SCALE_ELEMENT:
                    guiState = scaleElement(x, y);
                    break;
            }
        invalidate();
        repaint();
    }

    /**
     * boolean method to check, if form is clicked
     * @param        x x-coordinate of button click
     * @param        y y-coordinate of button click
     */

    private boolean processCallablesClick (int x, int y) throws IOException {
        for (Callable c : callableList) {
            if(c.mouseClick(x, y)) {

                return true;
            }
        }
        return false;
    }

    /**
     * ask user the question to be answered in text button
     * @param        question to be answered
     */
    public String askUser (String question) {
        return JOptionPane.showInputDialog(question);
    }

}


