package pancake;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

public class Paint extends Frame {
    public Paint () {

        board.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                try {
                    board.mousePressed(e.getX(),e.getY());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                board.mouseReleased(e.getX(),e.getY());
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("pancake");
        frame.setContentPane(new Paint().mainRoot);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private JPanel mainRoot;
    private Board board;


}
