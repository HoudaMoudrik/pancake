package pancake.button;


import pancake.Board;

import java.awt.*;
import java.io.IOException;
/**
 * Represent button to  save drawn shapes
 */
public class ButtonSave extends ButtonBase {

    /**
     *     define the used bord
     */
    private Board myBoard;

    /**
     * Create a valid save button
     * @param        topLeftX the x-coordinate of the top corn
     * @param        topLeftY the y-coordinate of the top corn
     * @param        height  the height of rectangle
     * @param        myBoard  Board, which is used as background
     */
    public ButtonSave (int topLeftX, int topLeftY, int height, Board myBoard) {
        super(topLeftX, topLeftY, height, height);
        this.myBoard = myBoard;
    }

    /**
     *     check if the mouse clicked in the button
     * @param         x the x-coordinate of the mouse-click
     * @param         y the y-coordinate of the mouse-click
     */
    @Override
    public boolean mouseClick (int x, int y) throws IOException {

            if (checkIfMouseInArea(x, y)) {
              myBoard.saveTheForm();
                return true;
            }

        return false;
    }

    /**
     * draw a new rectangle object based on scalar number
     * @param         g <code>Graphics</code> object
     */
    @Override
    public void paint (Graphics g) {
        super.paint(g);
        g.setColor(formColor);
        g.drawString("S",topLeftX+7,topLeftY+14);
    }

}
