package pancake.button;

import pancake.api.Callable;
import pancake.api.Drawable;

import java.awt.*;
import java.awt.Point;

/**
 * Base class for all button. can draw background of all button
 */
public abstract class ButtonBase implements Drawable, Callable {
    /** define and initialize the used color of content */
    protected Color backgroundColor =new Color(116, 60, 100, 91);
    protected Color contoursColor=new Color(0,0,0);
    /** define and initialize the content of button */
    protected Color formColor =new Color(0,0,0);
    /** define and initialize the x-coordinate of the top corn */
    protected int topLeftX;
    /** define and initialize the y-coordinate of the top corn */
    protected int topLeftY;
    /** define and initialize the height of rectangle */
    protected int height;
    /** define and initialize the width of rectangle */
    protected int width;

    /**
     * Create a valid circle object
     * @param        topLeftX the x-coordinate of the top corn
     * @param        topLeftY the y-coordinate of the top corn
     * @param        height  the height of rectangle
     * @param        width  the width of rectangle
     */
    public ButtonBase (int topLeftX, int topLeftY, int height, int width) {
        this.topLeftX = topLeftX;
        this.topLeftY = topLeftY;
        this.height = height;
        this.width = width;
    }

    /**
     *     check if the mouse clicked in the button
     * @param         x the x-coordinate of the mouse-click
     * @param         y the y-coordinate of the mouse-click
     */
    @Override
    public boolean checkIfMouseInArea(int x, int y){
        int  topLeftX= this.topLeftX;
        int  topLeftY= this.topLeftY;
        int  H= this.height+topLeftY;
        int  W= this.width+topLeftX;
        if(x>topLeftX && x<W && y>topLeftY && y<H){
            return true;
        }
        return false;
    }

    /**
     * draw a new rectangle object based on scalar number
     * @param         g <code>Graphics</code> object
     */
    @Override
    public void paint (Graphics g) {
        g.setColor(backgroundColor);
        g.fill3DRect(topLeftX,topLeftY,width,height, true);
        g.setColor(contoursColor);
        g.drawRect(topLeftX,topLeftY,width,height);
    }

    /**
     * move rectangle by offset coordinate x and y
     */
    @Override
    public void move (int x, int y) {}

    /**
     * scale or change the size of selected circle object based on scalar number
     * @param         scalar a double number used to scale element
     *
     */
    @Override
    public void scale (double scalar) { }

    /**
     *     The set method takes a parameter (newContourColor) and assigns it to the color variable.
     *     The this keyword is used to refer to the current object.
     * @param         newContourColor new value of text object
     */
    @Override
    public void setContourColor (Color newContourColor) {
        this.contoursColor =newContourColor;
    }

    /**
     *     The set method takes a parameter (newText) and assigns it to the text variable.
     *     The this keyword is used to refer to the current object.
     * @param         newText new value of text object
     */
    @Override
    public void setText (String newText) { }

    /**
     *     The set method takes a parameter (newContentColor) and assigns it to the color variable.
     *     The this keyword is used to refer to the current object.
     * @param         newContentColor new value of text object
     */
    @Override
    public void setBackgroundColor (Color newContentColor) {

    }

    /**
     * search the center of form
     */
    @Override
    public Point searchCenter () {
        return null;
    }

    /**
     * get method returns the value of the variable color (content)
     */
    @Override
    public Color getBackgroundColor () {
        return null;
    }

    /**
     * get method returns the value of the variable color (contour)
     */
    @Override
    public Color getContourColor () {
        return null;
    }

}
