package pancake.button;


import pancake.Board;

import java.awt.*;

/**
 * Draws a button to scale a selected shape
 */
public class ButtonScale extends ButtonBase  {

    /**
     *     define the used bord
     */
    private Board myBoard;

    /**
     * Create a valid circle object
     * @param        topLeftX the x-coordinate of the top corn
     * @param        topLeftY the y-coordinate of the top corn
     * @param        height  the height of rectangle
     * @param        myBoard  Board, which is used as background
     */

    public ButtonScale ( int topLeftX, int topLeftY, int height, Board myBoard) {
        super(topLeftX, topLeftY, height, height);
        this.myBoard = myBoard;
    }

    /**
     *     check if the mouse clicked in the button
     * @param         x the x-coordinate of the mouse-click
     * @param         y the y-coordinate of the mouse-click
     */
    @Override
    public boolean mouseClick (int x, int y) {

            if (checkIfMouseInArea(x, y)) {
                myBoard.chooseElementToBeScaled();
                return true;
            }

        return false;
    }

    /**
     * draw a new rectangle object based on scalar number
     * @param         g <code>Graphics</code> object
     */
    @Override
    public void paint (Graphics g) {
        super.paint(g);
        g.setColor(formColor);
        g.drawRect(topLeftX+1,topLeftY+13,height*1/3,height*1/3);
        g.drawRect(topLeftX+3,topLeftY+2,height*3/4,height*3/4);
        g.drawLine(topLeftX+9,topLeftY+13,topLeftX+16,topLeftY+4);
        g.drawLine(topLeftX+16,topLeftY+6,topLeftX+12,topLeftY+6);
        g.drawLine(topLeftX+16,topLeftY+6,topLeftX+16,topLeftY+8);
    }




}
