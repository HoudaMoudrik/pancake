package pancake.button;

import pancake.Board;

import java.awt.*;

public class ButtonArrow extends ButtonBase {
    /**
     * define the used bord
     */
    private Board myBoard;
    private int arrayX[] = {topLeftX + 18, topLeftX + 11, topLeftX + 11};
    private int arrayY[] = {topLeftY + 11, topLeftY + 17, topLeftY + 4};

    /**
     * Create a valid circle object
     *
     * @param topLeftX the x-coordinate of the top corn
     * @param topLeftY the y-coordinate of the top corn
     * @param height   the height of rectangle
     * @param myBoard  Board, which is used as background
     */
    public ButtonArrow (int topLeftX, int topLeftY, int height, int width, Board myBoard) {
        super(topLeftX, topLeftY, height, width);
        this.myBoard = myBoard;
    }

    /**
     * draw a new rectangle object based on scalar number
     *
     * @param g <code>Graphics</code> object
     */
    @Override
    public void paint (Graphics g) {
        super.paint(g);
        g.setColor(formColor);
        g.fillRect(topLeftX + 5, topLeftY + 8, width * 1 / 2, height * 1 / 3);
        g.fillPolygon(arrayX, arrayY, 3);
    }

    /**
     * check if the mouse clicked in the button
     *
     * @param x the x-coordinate of the mouse-click
     * @param y the y-coordinate of the mouse-click
     */

    @Override
    public boolean mouseClick (int x, int y) {

        if (checkIfMouseInArea(x, y)) {
            myBoard.startDrawArrow();
            return true;
        }

        return false;
    }
}
