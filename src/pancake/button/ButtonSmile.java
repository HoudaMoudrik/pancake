package pancake.button;


import pancake.Board;

import java.awt.*;

/**
 * Draws a button to start new smiley
 */
public class ButtonSmile extends ButtonBase {

    /**
     *     define the used bord
     */
    private Board myBoard;

    /**
     * Create a valid circle object
     * @param        topLeftX the x-coordinate of the top corn
     * @param        topLeftY the y-coordinate of the top corn
     * @param        height  the height of rectangle
     * @param        myBoard  Board, which is used as background
     */
    public ButtonSmile (int topLeftX, int topLeftY, int height, Board myBoard) {
        super(topLeftX, topLeftY, height, height);
        this.myBoard = myBoard;
    }

    /**
     *     check if the mouse clicked in the button
     * @param         x the x-coordinate of the mouse-click
     * @param         y the y-coordinate of the mouse-click
     */
    @Override
    public boolean mouseClick (int x, int y) {

            if (checkIfMouseInArea(x, y)) {
                myBoard.startDrawsmile();
                return true;
            }

        return false;
    }

    /**
     * draw a new rectangle object based on scalar number
     * @param         g <code>Graphics</code> object
     */
    @Override
    public void paint (Graphics g) {
        //540, 66,
        super.paint(g);
        g.setColor(formColor);
        g.drawOval(topLeftX+2,topLeftY+2,15,15);
        g.drawLine(topLeftX+6,topLeftY+8,topLeftX+6,topLeftY+8);
        g.drawLine(topLeftX+12,topLeftY+8,topLeftX+12,topLeftY+8);
        g.drawArc(topLeftX+6,topLeftY+12,6,3,0,180);
    }

}
