package pancake.button;



import pancake.Board;

import java.awt.*;


/**
 * Draws a button to start new move
 */
public class ButtonMove extends ButtonBase  {

    /**
     *     define and instantiate the different coordinates of button move
     */
    private  int xVerschiebung1[] = {topLeftX+1, topLeftX+5, topLeftX+6};
    private  int yVerschiebung1[] = {topLeftY+9, topLeftY+5, topLeftY+12};

    private int xVerschiebung2[] = {topLeftX+10, topLeftX+7, topLeftX+13};
    private int yVerschiebung2[] = {topLeftY, topLeftY+4, topLeftY+4};

    private  int xVerschiebung3[] = {topLeftX+15, topLeftX+19, topLeftX+15};
    private  int yVerschiebung3[] = {topLeftY+6, topLeftY+8, topLeftY+12};

    private int xVerschiebung4[] = {topLeftX+6, topLeftX+10, topLeftX+13};
    private int yVerschiebung4[] = {topLeftY+14, topLeftY+17, topLeftY+14};

    /**
     *     define the used bord
     */
    private  Board myBoard;

    /**
     * Create a valid move button
     * @param        topLeftX the x-coordinate of the top corn
     * @param        topLeftY the y-coordinate of the top corn
     * @param        height  the height of rectangle
     * @param        myBoard  Board, which is used as background
     */
    public ButtonMove (int topLeftX, int topLeftY,int height, Board myBoard) {
        super(topLeftX, topLeftY, height, height);

        this.myBoard = myBoard;
    }

    /**
     *     check if the mouse clicked in the button
     * @param         x the x-coordinate of the mouse-click
     * @param         y the y-coordinate of the mouse-click
     */
    @Override
    public boolean mouseClick (int x, int y) {

        if (checkIfMouseInArea(x, y)) {
            myBoard.chooseElementToBeDraged();
            return true;
        }
        return false;
    }

    /**
     * draw a new rectangle object based on scalar number
     * @param         g <code>Graphics</code> object
     */
    @Override
    public void paint (Graphics g) {
        super.paint(g);
        g.setColor(formColor);
        g.drawLine(topLeftX+10,topLeftY,topLeftX+10,topLeftY+17);
        g.drawLine(topLeftX,topLeftY+9,topLeftX+20,topLeftY+9);
        g.fillPolygon(xVerschiebung1,yVerschiebung1,3);
        g.fillPolygon(xVerschiebung2,yVerschiebung2,3);
        g.fillPolygon(xVerschiebung3,yVerschiebung3,3);
        g.fillPolygon(xVerschiebung4,yVerschiebung4,3);
    }

}
