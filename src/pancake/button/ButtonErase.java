package pancake.button;

import pancake.Board;

import java.awt.*;


/**
 * button to erase a selected shape
 */
public class ButtonErase extends ButtonBase {
    /*define the used bord*/
    private Board myBoard;

    /**
     * Create a valid circle object
     * @param        topLeftX the x-coordinate of the top corn
     * @param        topLeftY the y-coordinate of the top corn
     * @param        height  the height of rectangle
     * @param        myBoard  Board, which is used as background
     */
    public ButtonErase (int topLeftX, int topLeftY, int height, Board myBoard) {
        super(topLeftX, topLeftY, height, height);
        this.myBoard = myBoard;
    }
    /**
     *     check if the mouse clicked in the button
     * @param         x the x-coordinate of the mouse-click
     * @param         y the y-coordinate of the mouse-click
     */
    @Override
    public boolean mouseClick (int x, int y) {
        if(checkIfMouseInArea(x,y)) {
            myBoard.beginWithErasing();
            return true;
        }
        return false;
    }
    /**
     * draw a new rectangle object based on scalar number
     * @param         g <code>Graphics</code> object
     */
    @Override
    public void paint (Graphics g) {
        super.paint(g);
        g.setColor(formColor);

    }

}
