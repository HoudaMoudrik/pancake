package pancake.button;


import pancake.Board;

import java.awt.*;


/**
 * Draws a button to change the color of the shape
 */
public class ButtonPaint extends ButtonBase {
    /**
     *     define and instantiate the different coordinates of button paint
     */
    private  int arrayX[] = {topLeftX+3, topLeftX+14, topLeftX+10};
    private  int arrayY[] = {topLeftY+18, topLeftY+10, topLeftY+4};
    /**
     *     define the used bord
     */
    private  Board myBoard;

    /**
     * Create a valid circle object
     * @param        topLeftX the x-coordinate of the top corn
     * @param        topLeftY the y-coordinate of the top corn
     * @param        height  the height of rectangle
     * @param        myBoard  Board, which is used as background
     */
    public ButtonPaint ( int topLeftX, int topLeftY,int height, Board myBoard) {
        super(topLeftX, topLeftY, height, height);
        this.myBoard = myBoard;
    }

    /**
     *     check if the mouse clicked in the button
     * @param         x the x-coordinate of the mouse-click
     * @param         y the y-coordinate of the mouse-click
     */
    @Override
    public boolean mouseClick (int x, int y) {
        if(checkIfMouseInArea(x,y)) {
            myBoard.chooseTheColor();
            return true;
        }
        return false;
    }

    /**
     * draw a new rectangle object based on scalar number
     * @param         g <code>Graphics</code> object
     */
    @Override
    public void paint (Graphics g) {
        super.paint(g);
        g.setColor(formColor);
        g.fillPolygon(arrayX,arrayY,3);
        g.drawLine(topLeftX+14,topLeftY+8,topLeftX+18,topLeftY+6);
        g.drawLine(topLeftX+11, topLeftY+8, topLeftX+16,topLeftY+3);
        g.drawLine(topLeftX+9, topLeftY+5, topLeftX+13, topLeftY+1);
    }

}
