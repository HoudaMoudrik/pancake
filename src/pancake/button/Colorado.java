package pancake.button;

import pancake.api.*;
import pancake.form.Rectangle;


import java.awt.*;
import java.util.ArrayList;

/**
 * Represents a palette of eight colors. The user can pick content and contour color
 */
public class Colorado  extends ButtonBase implements Drawable,Callable {

    /** define and initialize the used color of content */
    private Color contentColor= new Color(0,0,0);
    /** define and initialize the used color of  contour */
    private Color contourColor = new Color(0,0,0);
    /** define and initialize the color of content saved in order to be use in selected form */
    private Color savedContentColor= new Color(0,0,0);
    /** define and initialize the color of contour saved in order to be use in selected form */
    private   Color savedContourColor= new Color(0,0,0);


    /* define and instantiate of arraylist of squares used in palette */
    private  ArrayList<Drawable> square = new ArrayList<Drawable>();

    /**
     * Create a valid colorado object
     * @param        topLeftX the x-coordinate of the top corn
     * @param        topLeftY the y-coordinate of the top corn
     * @param        size  of square
     */
    public Colorado (int topLeftX, int topLeftY, int size) {
        super(topLeftX,topLeftY,size,size);

        square.add(new Rectangle(topLeftX,topLeftY,size,size));
        square.add(new Rectangle(topLeftX+size,topLeftY,size,size));
        square.add(new Rectangle(topLeftX+2*size,topLeftY,size,size));
        square.add(new Rectangle(topLeftX+3*size,topLeftY,size,size));
        square.add(new Rectangle(topLeftX+4*size,topLeftY,size,size));

        square.add(new Rectangle(topLeftX,topLeftY+size,size,size));
        square.add(new Rectangle(topLeftX+size,topLeftY+size,size,size));
        square.add(new Rectangle(topLeftX+2*size,topLeftY+size,size,size));
        square.add(new Rectangle(topLeftX+3*size,topLeftY+size,size,size));
        square.add(new Rectangle(topLeftX+4*size,topLeftY+size,size,size));

        square.add(new Rectangle(topLeftX-2*size,topLeftY,2*size,2*size));
        square.add(new Rectangle(topLeftX-5*size,topLeftY,2*size,2*size));

        square.get(0).setBackgroundColor(Color.RED);
        square.get(1).setBackgroundColor(Color.DARK_GRAY);
        square.get(2).setBackgroundColor(Color.CYAN);
        square.get(3).setBackgroundColor(Color.ORANGE);
        square.get(4).setBackgroundColor(Color.magenta);

        square.get(5).setBackgroundColor(Color.blue);
        square.get(6).setBackgroundColor(Color.lightGray);
        square.get(7).setBackgroundColor(Color.black);
        square.get(8).setBackgroundColor(Color.GREEN);
        square.get(9).setBackgroundColor(Color.GRAY);
    }

    /**
     * draw a thw palette made with several colors
     * @param         g <code>Graphics</code> object
     */
    @Override
    public void paint (Graphics g) {
        for(Drawable d : square){
            d.paint(g);
        }
    }

    /**
     *     getter for saved color of contour
     */
    public Color getSavedContourColor () {
        return savedContourColor;
    }

    /**
     *     getter for saved color of content
     */
    public Color getSavedContentColor () {
        return savedContentColor;
    }

    /**
     *     The set method takes a parameter (newContentColor) and assigns it to the color variable.
     *     The this keyword is used to refer to the current object.
     * @param         newContentColor new value of text object
     */
    @Override
    public void setBackgroundColor (Color newContentColor) {
       this.contentColor=newContentColor;
    }

    /**
     *     The set method takes a parameter (newContourColor) and assigns it to the color variable.
     *     The this keyword is used to refer to the current object.
     * @param         newContourColor new value of text object
     */
    @Override
    public void setContourColor (Color newContourColor) {
      this.contourColor =newContourColor;
    }

    /**
     *     getter for color of content
     */
    @Override
    public Color getBackgroundColor () {
        return this.contentColor;
    }

    /**
     *     getter for color of content
     */
    @Override
    public Color getContourColor () {
        return this.contourColor;
    }

    /**
     * boolean method to check, if one of the defined button is clicked
     * @param        x x-coordinate of button click
     * @param        y y-coordinate of button click
     */
    @Override
    public boolean checkIfMouseInArea (int x, int y) {
        for(Drawable d : square){
          d.checkIfMouseInArea(x,y);
          if(d.checkIfMouseInArea(x,y)){
              return true;
          }
        }
        return false;
    }

    /**
     * boolean method to check, if form is clicked
     * @param        x x-coordinate of button click
     * @param        y y-coordinate of button click
     */
    @Override
    public boolean mouseClick (int x, int y) {
        for(Drawable d : square){

            boolean check=d.checkIfMouseInArea(x,y);
            if(check){
                savedContourColor = savedContentColor;
                savedContentColor = d.getBackgroundColor();
                square.get(10).setBackgroundColor(savedContentColor);
                square.get(11).setBackgroundColor(savedContourColor);
                return true;
            }
        }
        return false;
    }
}
