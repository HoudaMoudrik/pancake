package pancake.api;

import java.awt.*;
import java.awt.Point;
/**
 * interface for everything that can be drawn
 */
public interface Drawable {
    public void paint(Graphics g) ;
    public void setBackgroundColor (Color c);
    public void setContourColor (Color c);
    public void setText (String text);
    public Color getBackgroundColor ();
    public Color getContourColor ();
    public boolean checkIfMouseInArea(int x, int y);
    public void move(int x, int y);
    public void scale (  double scalar);
    public Point searchCenter();

}
