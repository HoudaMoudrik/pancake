package pancake.api;

import java.io.FileWriter;
import java.io.IOException;
/**
 * interface for everything that can be saved
 */
public interface Savable extends Drawable {
    public void save(FileWriter writer) throws IOException;
    public void load (String[] part);
}
