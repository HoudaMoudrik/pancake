package pancake.api;

import java.io.IOException;

/**
 * interface for everything that react to mouse input
 */
public interface Callable {
    public boolean mouseClick (int x, int y) throws IOException;

}
